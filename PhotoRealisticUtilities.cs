﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NetVips;
using ImageProcesingUtils;
using ImageProcessing;
using ImageProcessingCore;
using ImageProcessingUtils;

namespace PhotorealisticImplementation
{
    class PhotoRealisticUtilities
    {
        public static string textureFilesPath = @"D:\Work\PhotoRealistic\Goodtextures";

        public static NetVips.Image CreateOutlineofImage(string fname)
        {
            NetVips.Image img = NetVips.Image.NewFromFile(fname, access: NetVips.Enums.Access.Sequential).Colourspace("b-w");
            if (img.HasAlpha())
            {
                img = img.Flatten();
            }
            var rightShiftKernel = Image.NewFromArray(new[,]
                                            {
                                                {-1, 1}
                                            });
            var leftShiftKernel = Image.NewFromArray(new[,]
            {
                { 1, 1},
                { -1, -1}

            });
            // Creating Outline of an Image
            var outlineRight = img.Conv(rightShiftKernel).Abs();
            var outlineLeft = img.Conv(leftShiftKernel).Abs();
            outlineRight = outlineRight + outlineLeft;
            return outlineRight;
        }
        public static NetVips.Image CreateNormalMap(string filename, double intensity)
        {
            var i = NetVips.Image.NewFromFile(filename, access:Enums.Access.Random) / 255;
            var identityImg = NetVips.Image.Black(i.Width, i.Height, 1) + 1;
            var bands = i.Bandsplit();
            var greyImg = bands[0] * 0.3 + bands[1] * 0.6 + bands[2] * 0.1;
            var x_kernel = Image.NewFromArray(new[,]
                                {
                                                {1, 0.0, -1.0},
                                                {2, 0.0, -2},
                                                {1, 0.0, -1.0}
                                            });
            var y_kernel = Image.NewFromArray(new[,]
                    {
                                                {-1, -2, -1},
                                                {0, 0, 0},
                                                {1, 2, 1}
                                            });

            var grad_x = greyImg.Conv(x_kernel, precision: "integer");
            var grad_y = greyImg.Conv(y_kernel);

            var max_value = grad_x.Max();
            if (max_value < grad_y.Max())
            {
                max_value = grad_y.Max();
            }

            intensity = 1 / intensity;
            var strength = max_value / (max_value * intensity);
            var map_x = grad_x / max_value;
            var map_y = grad_y / max_value;
            var map_z = identityImg / strength;

            var norm = (map_x.Pow(2) + map_y.Pow(2) + map_z.Pow(2)).Pow(0.5);
            map_x = map_x.Divide(norm);
            map_y = map_y.Divide(norm);
            map_z = map_z.Divide(norm);

            map_x = map_x.Bandjoin(map_y);
            map_x = map_x.Bandjoin(map_z);

            map_x = map_x * 0.5;
            map_x = map_x + 0.5;
            map_x = map_x * 255;
            return map_x;

        }

        public static NetVips.Image CreateBumpMap(string filename)
        {

            var img = NetVips.Image.NewFromFile(filename, access:Enums.Access.Sequential);
            if (img.HasAlpha())
            {
                img = img.Flatten();
            }
            img = img.Colourspace("b-w");

            var kernel_smallOne = Image.NewFromArray(new[,]
                                {
                                                {0.0, 0.0, 1.0},
                                                {0.0, 0.0, 0.0},
                                                {0.0, 0.0, -1.0}
                                            }, scale: 0.25, offset: 1);
            var bump = img.Conv(kernel_smallOne, precision: "float") + 128;
            bump = (bump > 200).Ifthenelse(200, bump);
            return bump;
        }

        public static NetVips.Image ApplyTextureinDesign(string filename, List<Tuple<int[], int, int, int>> photorealisticData)
        {

            string[] textureNames = new string[] { textureFilesPath + "\\custom.jpg", textureFilesPath + "\\loop.jpg", textureFilesPath + "\\silk.jpg", textureFilesPath + "\\wool.jpg" };
            NetVips.Image originalImage = NetVips.Image.NewFromFile(filename, access: Enums.Access.Sequential);

            if (originalImage.HasAlpha()) { originalImage = originalImage.Flatten(); }

            var textureAdded = originalImage;
            var bumpIndex = NetVips.Image.Black(originalImage.Width, originalImage.Height, 3) + 255;

            for (int i = 0; i < photorealisticData.Count; i++)
            {
                //textureAdded = (originalImage.Equal(prDetails[i].Item1)).Ifthenelse(textureAdded + textureImages[prDetails[i].Item2] - 128 , textureAdded);
                textureAdded = (originalImage.Equal(photorealisticData[i].Item1)).Ifthenelse(textureAdded + NetVips.Image.NewFromFile(textureNames[photorealisticData[i].Item2], access: "sequential").Crop(10, 10, originalImage.Width, originalImage.Height) - 128, textureAdded);

                //bumpIndex = (originalImage.Equal(prDetails[i].Item1)).Ifthenelse(prDetails[i].Item3 * 51, bumpIndex);
            }
            return textureAdded;
        }
        public static List<Tuple<int[], int, int, int>> GeneratePhotoRealisticData(string fname)
        {
            Surface img = SurfaceUtils.SurfaceFromFile(fname);
            var colors = img.GetUsedColorInfo();
            List<Tuple<int[], int, int, int>> prColorDetails = new List<Tuple<int[], int, int, int>>();
            Random rnd = new Random();
            for (int i = 0; i < colors.Count; i++)
            {
                int textureFile = rnd.Next(0, 3);
                int pileHeight = rnd.Next(1, 4);
                int carvingInfo = rnd.Next(1, 3);
                prColorDetails.Add(Tuple.Create(new[] { (int)colors[i].DisplayColor.R, (int)colors[i].DisplayColor.G, (int)colors[i].DisplayColor.B }, textureFile, 0, 0));
            }
            return prColorDetails;
        }

    }
}
