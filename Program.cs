using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhotorealisticImplementation
{
    class Program
    {
        public static string filename = @"D:\Work\PhotoRealistic\5c\5c4.png";
        public static string pathForTextreFiles = @"D:\Work\PhotoRealistic\Goodtextures";
        static void Main(string[] args)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            PhotoRealisticUtilities.textureFilesPath = pathForTextreFiles;

            // this list contains tuples where Item1 of tuple is color of design and Item2 is its correponding Texture File and Item3 and 4 are Pile Height and Carving Information 
            List<Tuple<int[], int, int, int>> photoRealisticDummyData = PhotoRealisticUtilities.GeneratePhotoRealisticData(fname: filename);
            NetVips.Image textureAdded = PhotoRealisticUtilities.ApplyTextureinDesign(filename: filename, photorealisticData: photoRealisticDummyData);
            textureAdded.WriteToFile("D:\\textureAdded.jpg");
            //var img = PhotoRealisticUtilities.CreateNormalMap(filename: filename, intensity: 5);

            Console.WriteLine(sw.ElapsedMilliseconds);
            Console.WriteLine("Finished");
            Console.ReadKey();

            // Go to http://aka.ms/dotnet-get-started-console to continue learning how to build a console app! 
        }
    }
}
